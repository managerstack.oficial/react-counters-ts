
// Define las acciones posibles y la interfaz correspondiente
export enum CounterActionType {
  INCREMENT = 'INCREMENT',
  DECREMENT = 'DECREMENT',
}