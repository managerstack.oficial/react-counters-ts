
import { Hero } from '../components/layout/Hero';
import  useDocumentTitle  from '../hooks/useDocumentTitle'

export function HomeController() {

  useDocumentTitle("Inicio")
  //Render the sections
  return (  <Hero /> )
}

