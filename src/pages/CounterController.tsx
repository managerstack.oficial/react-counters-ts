
import { useCounterAccess } from '../reducers/counter/hooks/useCounterAccess';
import { Counters } from '../components/counter/Counters'
import { CounterAdd } from '../components/counter/CounterAdd';
import { CounterHeader } from '../components/counter/CounterHeader';

import './Counter.scss';
import { CounterTotal } from '../components/counter/CounterTotal';
import  useDocumentTitle  from '../hooks/useDocumentTitle'


  
export function CounterController() {
  
  //Get general state
  useDocumentTitle("Contador")
  const {counterData, counterTotal, counterLength} = useCounterAccess();



  //Render the sections
  return (
        <div className='counter-controller px-4 py-12 mx-auto max-w-7xl sm:px-6 md:px-12 lg:px-24 lg:py-24'>
          <div className="flex flex-col w-full mb-12 text-center">

              {counterLength === 0 && <CounterHeader
                              title="¡Let's go!"
                              text='Presiona generar contador para iniciar la prueba'></CounterHeader> }

              {counterLength > 0 && <CounterHeader
                              title="¡Let's go!"
                              text='Presiona generar contador para agregar contadores'></CounterHeader> }

                {counterLength > 0 && <table className="counters">
                  <tbody> 
                  {
                      counterData.map((e) => 
                      <Counters 
                        key={e.id} 
                        id={e.id} 
                        value={e.value}></Counters>)
                  }
                  </tbody>

                  <CounterTotal 
                    counterTotal={counterTotal} 
                    counterLength={counterLength}></CounterTotal>

                </table> }

                <div className='counter-buttons'>
                  <CounterAdd />
                </div>
          </div>
        </div>
    )
}

