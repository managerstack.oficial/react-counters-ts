import { createSlice } from '@reduxjs/toolkit'
import { counterInitialState, counterReducers } from './counterReducerMethods'


//S.O.L.I.D (SRP) (OCP) (DIP)
//Redux configuration
//create the slice
export const counterSlice = createSlice({
  name: 'counter',
  initialState: counterInitialState,
  reducers: counterReducers,
})

// Action creators are generated for each case reducer function
export const { counterIncrement, counterDecrement, counterAdd, counterRemove } = counterSlice.actions

export default counterSlice.reducer