import { CounterState } from '../../utils/interfaces/counter/counter.interfaces'
import { PayloadAction } from '@reduxjs/toolkit'

//initial state in the slice
export const counterInitialState: CounterState[] = [];

//Create all methods in the global slice
export const counterReducers = {
  //increment the counter with a index
  counterIncrement: (state: CounterState[], action: PayloadAction<number>) => {
    return [
      ...state
      .map(e => {
        if (e.id === action.payload) 
          return {
            ...e,
            value: e.value + 1 
          }
        return e 
      })
    ]
  },
  //decrement the counter with a index
  counterDecrement: (state: CounterState[], action: PayloadAction<number>) => {
    return [
      ...state
      .map(e => {
        if (e.id === action.payload) 
          return {
            ...e,
            value: e.value - 1 
          }
        return e 
      })
    ]
  },
  //add new counter at the data
  counterAdd: (state: CounterState[]) => {
      return [
        ...state,
        {
          id:state.length, 
          value: 0
        }
      ]
  },
  //remove counter from data
  counterRemove: (state: CounterState[], action: PayloadAction<number>) => {
    let counter = 0;
    //filter the element and update the counter
    return state
    .filter((e) => e.id !== action.payload)
    .map((e)=> { return {id: counter++, value: e.value}});
    
  },
}
