import { useSelector } from 'react-redux'
import type { RootState } from '../../../store/store'

  
export function useCounterAccess() {

  //Get general state and store locally 
  const counterData = useSelector((state: RootState) => state.counter) || []
  //Get Lenght and store locally
  const counterLength: number = counterData.length;
  //Get total Counters locally
  let counterTotal: number = 0;
  if (counterLength > 0) 
    counterTotal = counterData.map(a => a.value).reduce(function(a, b){ return a + b;});

  return {
    counterData: counterData,
    counterTotal: counterTotal,
    counterLength: counterLength
  };
}