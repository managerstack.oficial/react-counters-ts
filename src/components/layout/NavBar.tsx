import { NavLink  } from "react-router-dom";
import './NavBar.scss'

export function NavBar() {

  return (
  <nav className="nav">
    <div className="container">
      <a href="#" className="flex items-center">
          <img src="https://flowbite.com/docs/images/logo.svg" className="h-6 mr-3 sm:h-9" alt="Contadores logo" />
          <span className="self-center text-xl font-semibold whitespace-nowrap dark:text-white">Contadores</span>
      </a>
      <div  id="navbar-default">
        <ul>
          <li>
            <NavLink 
              to="/"  
              className={({ isActive }) => (isActive ? 'active' : 'inactive') }    aria-current="page">Home</NavLink >
          </li>
          <li>
            <NavLink  
              to="/contadores" 
              className={({ isActive }) => (isActive ? 'active' : 'inactive') }>Contadores</NavLink >
          </li>
        </ul>
      </div>
    </div>
  </nav>)
}