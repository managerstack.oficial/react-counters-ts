import { Link } from "react-router-dom";
import './Footer.scss'

export const Footer = () =>{

  return (
    <footer className="footer">
        <span className="text">© 2023 Contadores. All Rights Reserved.
        </span>
        <ul >
            <li>
                <Link to="/">Inicio</Link>
            </li>
            <li>
                <Link to="/Contadores">Contadores</Link>
            </li>
        </ul>
    </footer>
  )

}