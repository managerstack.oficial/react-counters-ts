import { NavLink  } from "react-router-dom";
import './Hero.scss'

export const Hero = () => {

  return (
    <section>
        <div className="container-hero">
          <div className="flex_">
            
            <div className="icon_">
              <svg xmlns="http://www.w3.org/2000/svg"  width="24" height="24" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <circle cx="12" cy="12" r="9"></circle>
                <line x1="3.6" y1="15" x2="14.15" y2="15"></line>
                <line x1="3.6" y1="15" x2="14.15" y2="15" transform="rotate(72 12 12)"></line>
                <line x1="3.6" y1="15" x2="14.15" y2="15" transform="rotate(144 12 12)"></line>
                <line x1="3.6" y1="15" x2="14.15" y2="15" transform="rotate(216 12 12)"></line>
                <line x1="3.6" y1="15" x2="14.15" y2="15" transform="rotate(288 12 12)"></line>
              </svg>
            </div>

            <h1 className="title">
              ¡Bienvenidos a la página de presentación de nuestro emocionante proyecto!
            </h1>

            <p className="text">
              Como todos sabemos, los contadores son una herramienta esencial en muchas situaciones. Ya sea para contar el número de personas en un evento, el número de repeticiones en un ejercicio o simplemente para llevar la cuenta de los puntos en un juego, los contadores son una necesidad. Y es ahí donde nuestra aplicación de multicontadores entra en juego.
            </p>

            <NavLink to="/contadores" className="link" title="read more"> Ir al proyecto » </NavLink>
          </div>
        </div>
      </section>
  )
}