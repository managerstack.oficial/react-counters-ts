import { Outlet } from "react-router-dom";
import { Footer } from "./Footer";
import { NavBar } from "./NavBar";

const PublicLayout = () => {
  return (
    <>
      <NavBar />
      <div className="App">
        <Outlet />
      </div>
      <Footer />
    </>
  )
};

export default PublicLayout;