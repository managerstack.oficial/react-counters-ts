
import { useDispatch } from 'react-redux'
import { counterAdd } from '../../reducers/counter/counterReducer'
import { Button } from '../button/Button'

  
export function CounterAdd() {
  const dispatch = useDispatch()

  return (
      <Button 
        name="Generar nuevo contador" 
        handlerClick={() => dispatch(counterAdd())} 
        type="high extra mt-2"></Button>
  )
}