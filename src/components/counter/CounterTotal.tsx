

interface Props {
  counterLength:number,
  counterTotal: number
}

export function CounterTotal({counterLength, counterTotal}: Props) {
 
  return (
        <tfoot>
          <tr className="counter-element">
              
              <td className="text-center">

              </td>

              <td className="text-center">

              </td>

              <td className="text-center">

              </td>

              <td className='text-center'>
                {counterLength}
              </td>
              
              <td className='text-center value'>
                {counterTotal}
              </td>
          </tr>
        </tfoot>

  )
}