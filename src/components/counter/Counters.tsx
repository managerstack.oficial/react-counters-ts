
import { useDispatch } from 'react-redux'
import { CounterState } from '../../utils/interfaces/counter/counter.interfaces';
import { counterDecrement, counterIncrement, counterRemove } from '../../reducers/counter/counterReducer'

import { Button } from '../button/Button'
import { Circle } from '../desing/Circle';
import React from 'react';


function MemoCounters({id, value}: CounterState) {
  //Get the dispatchs
  const dispatch = useDispatch()

  return (

        <tr className="counter-element">
            
            <td className="text-center index">
              <Circle number={(id+1).toString()}></Circle>
            </td>

            <td className="text-center">
              <Button 
                name="Remover" 
                handlerClick={() => dispatch(counterRemove(id))} 
                type="low"></Button>
            </td>

            <td className="text-center">
              <Button 
                name="Decrementar" 
                handlerClick={() => dispatch(counterDecrement(id))} 
                type="low"></Button>
            </td>

            <td className='text-center'>
              <Button 
                name="Incrementar" 
                handlerClick={() => dispatch(counterIncrement(id))} 
                type="normal"></Button>
            </td>
            
            <td className='text-center value'>{value}</td>
        </tr>

  )
}

export const Counters = React.memo(MemoCounters);