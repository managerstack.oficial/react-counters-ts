

interface Props {
  title:string,
  text: string
}
  
export function CounterHeader({title, text}: Props ) {
  
  return (
      <div className="counter-header">
        <h3 className="max-w-5xl text-2xl font-bold leading-none tracking-tighter text-neutral-600 md:text-5xl lg:text-6xl lg:max-w-7xl">{title}</h3>
        <div className="max-w-xl mx-auto mt-8 text-base leading-relaxed text-center text-gray-500">{text}</div>
      </div>
  )
}