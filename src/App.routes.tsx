import { Routes, Route, Navigate } from 'react-router-dom';
import PublicLayout from './components/layout/PublicLayout';

import { HomeController } from './pages/HomeController';
import { CounterController } from './pages/CounterController';
export const AppRoutes = () => {
  return (
    
      <Routes>
        <Route path="/" element={<PublicLayout />}>
          
          <Route 
            path="/" 
            element={<HomeController />} ></Route>
          
          <Route 
            path="contadores" 
            element={<CounterController />} />
          
          <Route
            path="*"
            element={<Navigate replace to="/contadores" />}
          />

        </Route>
      </Routes>
      
  );
};